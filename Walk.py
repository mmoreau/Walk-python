#!/usr/bin/python3

import os
import sys
import argparse

class Walk:

	@classmethod
	def SearchExt(cls, path: str, extensions: list) -> None:

		"""Searches for specific file extensions in the specified location."""

		if os.path.isdir(path):
			extensions = [x.lower() for x in extensions if isinstance(x, str)] if extensions else [""]
			[sys.stdout.write(f"{os.path.join(root, filename)}\n") if f"{'.'.join(filename.split('.')[1:]).lower()}" in extensions else _ for root, _, files in os.walk(path) for filename in files]


try:	
	if len(sys.argv) >= 5:
	
		parser = argparse.ArgumentParser()
		parser.add_argument("-p", "--path", type=str)
		parser.add_argument("-e", "--ext", nargs="*")

		args = parser.parse_args()

		_path = args.path
		_ext = args.ext

		if _path and _ext:
			Walk.SearchExt(_path, _ext)
except:
	pass
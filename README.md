# Walk

Traverses paths passed in parameter to find file extensions

## Note

Use a version of **Python3.6+** to use this script, otherwise you'll have to reorder the instructions to versions inside Python3.6+.

## Terminal

### **Search only one extension**

Windows
```
    Walk.py -p C:\ -e exe
    Walk.py -p C:\MyDirectory\MySubDirectory -e exe
```

Linux & MacOS
```
    python3 Walk.py -p / -e pdf
    python3 Walk.py -p /MyDirectory/MySubDirectory -e pdf
```

---

### **Search multiple extensions**

Windows
```
    Walk.py -p C:\ -e exe py dll
    Walk.py -p C:\MyDirectory\MySubDirectory -e exe py dll
```

Linux & MacOS
```
    Walk.py -p / -e exe py so pdf
    Walk.py -p /MyDirectory/MySubDirectory -e py so pdf
```

---

### **Search files that do not have extensions**

Windows
```
    Walk.py -p C:\ -e ""
    Walk.py -p C:\MyDirectory\MySubDirectory -e ""
```

Linux & MacOS
```
    Walk.py -p / -e ""
    Walk.py -p ~ -e ""
    Walk.py -p /MyDirectory/MySubDirectory -e ""
```

---

### **Searches for files that have multiple extensions**

Windows
```
    Walk.py -p C:\ -e html.php
    Walk.py -p C:\MyDirectory\MySubDirectory -e html.php
```

Linux & MacOS
```
    Walk.py -p / -e html.php
    Walk.py -p ~ -e html.php
    Walk.py -p /MyDirectory/MySubDirectory -e html.php
```

---

### **Store search results in a file**

Windows
```
    Walk.py -p C:\ -e exe > data.txt
    Walk.py -p C:\MyDirectory\MySubDirectory -e exe > data.txt
```

Linux & MacOS
```
    Walk.py -p / -e so pdf html.php > data.txt
    Walk.py -p ~ -e so pdf html.php > data.txt
    Walk.py -p /MyDirectory/MySubDirectory -e so pdf html.php > data.txt
```

## Script

You can consult the file "script.py" for examples of use.

```python
    #!/usr/bin/python3
    
    import os
    from Walk import Walk
    
    # Windows 
    Walk.SearchExt("C:\\", ["exe"])
    Walk.SearchExt("C:\\", [""])
    Walk.SearchExt("C:\\", ["exe", "dll"])
    Walk.SearchExt("C:\\", ["html.php", "dll"])
    Walk.SearchExt("C:\\MyDirectory\\MySubDirectory", ["exe"])
    
    # Linux & MacOS
    Walk.SearchExt("/", ["mp4"])
    Walk.SearchExt("/", [""])
    Walk.SearchExt("/", ["py", "so"])
    Walk.SearchExt("/", ["html.php", "pdf"])
    Walk.SearchExt(os.path.expanduser("~"), ["mp4", "mp3", "jpg", "jpeg", "png", "gif"])
    Walk.SearchExt("/MyDirectory/MySubDirectory", ["mp4"])
    Walk.SearchExt(os.path.join(os.path.expanduser("~"), "Desktop"), ["pdf", "py"])
```

## Performance

```python
    import os
    import timeit
    from Walk import Walk
    
    print(timeit.timeit('Walk.SearchExt("/", ["py", "pdf", "so"])', globals=globals(), number=1))
    print(timeit.timeit('Walk.SearchExt(os.path.expanduser("~"), ["mp4", "mp3", "jpg", "jpeg", "png", "gif"])', globals=globals(), number=1))
```

## Todo
* [ ] Search for folder names

## Contact

Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered 

* **Twitter :** https://twitter.com/mxmmoreau
* **LinkedIn :** https://www.linkedin.com/in/mxmoreau/
#!/usr/bin/python3

import os
import sys
from Walk import Walk

# .:: Windows ::. # 

# Search only one extension ? 
#Walk.SearchExt("C:\\", ["exe"])

# Search multiple extensions ?
#Walk.SearchExt("C:\\", ["exe", "dll"])

# Longer path
#Walk.SearchExt("C:\\MyDirectory\\MySubDirectory", ["exe"])


# .:: Linux & Mac ::. #

# Search only one extension ? 
#Walk.SearchExt("/", ["py"])
#Walk.SearchExt(".", ["py"])

# Displays files that don't have an extension

#Walk.SearchExt(os.path.expanduser("~"), [""])

# Search multiple extensions ?
#Walk.SearchExt(".", ["py", "so"])
#Walk.SearchExt("/", ["py", "so"])
#Walk.SearchExt(os.path.join(os.path.expanduser("~"), "Bureau"), ["pdf", "py"])

# Longer path
#Walk.SearchExt("/MyDirectory/MySubDirectory", ["py"])

if sys.platform.startswith("win32"):
	os.system("pause")